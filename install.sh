#!/bin/sh

installifexists () {
  if type $1 >/dev/null; then
    printf "$1 found. Installing service dir to /etc/runit/sv...\n"
    if ! [ -d "/etc/runit/sv/$1" ]; then
      sudo cp -rn $1 /etc/runit/sv/
    fi
    if [ -d "config/$1" ]; then
      printf "and config to /etc...\n"
      sudo cp -rn config/$1/ /etc/
    fi
  fi
}

printf "Installing services if they exist...\n\n\n"

for i in `ls`; do
  [ -f "$i" ] && continue
  [ "$i" = "config" ] && continue
  printf "$i...\n"
  installifexists "$i"
  printf "\n"
done
